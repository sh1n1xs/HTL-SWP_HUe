using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic;

/*
 *TO-DO
 * % for all stock strats
 * pass everything as argument
 * maybe: fix graph
 * 
 */

namespace stocksSimulator9000
{
    public class stockSim
    {
        private SortedDictionary<DateTime, StockData> _sortedData;
        private string _stockSymbol;

        private int _stockDepot = 0;
        private decimal _bank = 100000;
        
        private StockStrategy _strat = StockStrategy.Avg;

        public SortedDictionary<DateTime, changeTableData> changeTable = new SortedDictionary<DateTime, changeTableData>();
        
        public decimal bankDepot
        {
            get
            {
                return this._bank;
            }
        }
        
        private decimal _startBank = 100000;
        
        public StockStrategy StrategyForSim
        {
            get
            {
                return this._strat;
            }
            set
            {
                this._strat = value;
            }
        }
        
        public stockSim(Dictionary<DateTime, StockData> data, string stockSymbol)
        {
            this._sortedData = new SortedDictionary<DateTime, StockData>(data);
            this._stockSymbol = stockSymbol;
            this._bank = 100000;
            this._startBank = 100000;
        }
        
        public stockSim(Dictionary<DateTime, StockData> data, string stockSymbol, decimal bank)
        {
            this._sortedData = new SortedDictionary<DateTime, StockData>(data);
            this._stockSymbol = stockSymbol;
            this._bank = bank;
            this._startBank = bank;
        }

        public void RunSimu(DateTime startDate)
        {
            this.changeTable.Clear(); //clears change table
            this._bank = this._startBank;
            Console.WriteLine("Running simulation for " + this._stockSymbol + Environment.NewLine);
            Console.Write("with the following strategy: ");
            switch (_strat)
            {
                case StockStrategy.Avg:
                    Console.WriteLine("200 average");
                    break;
                case StockStrategy.AvgMargin:
                    Console.WriteLine("200 average with 3% upper and lower bound");
                    break;
                case StockStrategy.BuyAndHold:
                    Console.WriteLine("Buy and hold");
                    break;
            }
            
            bool hasStarted = false;
            bool shouldSell = false;
            decimal tmpClose = 0;
            DateTime tmpKey = new DateTime();

            foreach (var item in this._sortedData)
            {
                if (item.Key > startDate)
                {
                    hasStarted = true;
                    if (this.StrategyForSim == StockStrategy.BuyAndHold) //if we do the buy and hold thing we buy one time....
                    {
                        hasStarted = false;
                        if (!shouldSell)
                        {
                            this.BuyStocks(item.Value.AdjustedClose);
                            shouldSell = true; //and then don't do anything until the loop is done
                            this.addToSimTable(item.Key, this._bank, this._stockDepot, StockDepotAction.Bought); //adds to change log table
                        }
                    }
                }
                if (hasStarted)
                {

                    this._stockDepot = Convert.ToInt32(Convert.ToDecimal(this._stockDepot) * item.Value.SplitCorrection);
                    if (!shouldSell)//we look for a day to buy
                    {
                        switch (this.StrategyForSim)
                        {
                            case StockStrategy.Avg:
                                //calculate if we should buy today here
                                if (item.Value.AdjustedClose > item.Value.AdjustedAvg)
                                {
                                    this.BuyStocks(item.Value.AdjustedClose);
                                    shouldSell = true;
                                    this.addToSimTable(item.Key, this._bank, this._stockDepot, StockDepotAction.Bought); //adds to change log table
                                }
                                break;
                            case StockStrategy.AvgMargin:
                                //calculate if we should buy today here
                                if (item.Value.AdjustedClose > (item.Value.AdjustedAvg * Convert.ToDecimal(1.03)))
                                {
                                    this.BuyStocks(item.Value.AdjustedClose);
                                    shouldSell = true;
                                    this.addToSimTable(item.Key, this._bank, this._stockDepot, StockDepotAction.Bought); //adds to change log table
                                }
                                break;
                            
                        }
                    } //we look for a day to sell
                    else
                    {
                        switch (this.StrategyForSim)
                        {
                            case StockStrategy.Avg:
                                if (item.Value.AdjustedClose < item.Value.AdjustedAvg)
                                {
                                    this.SellStock(item.Value.AdjustedClose);
                                    shouldSell = false;
                                    this.addToSimTable(item.Key, this._bank, this._stockDepot, StockDepotAction.Sold); //adds to change log table
                                }
                                break;
                            case StockStrategy.AvgMargin:
                                if (item.Value.AdjustedClose < (item.Value.AdjustedAvg * Convert.ToDecimal(0.97)))
                                {
                                    this.SellStock(item.Value.AdjustedClose);
                                    shouldSell = false;
                                    this.addToSimTable(item.Key, this._bank, this._stockDepot, StockDepotAction.Sold); //adds to change log table
                                }
                                break;
                        }
                    }
                    
                }
                //saves last data for last day
                tmpKey = item.Key;
                tmpClose = item.Value.AdjustedClose;
            }
            //in the last day we sell everything regardless
            this.SellStock(tmpClose);
            this.addToSimTable(tmpKey, this._bank, this._stockDepot, StockDepotAction.Bought); //adds to change log table
            this._bank = this._bank * 100;
            this._bank = Math.Floor(this._bank);
            this._bank = this._bank / 100;
            
            Console.WriteLine("Old bank account: " + this._startBank);
            Console.WriteLine("Current bank account: " + this._bank);
            //calculates %
            double percentage = Convert.ToDouble(this._bank / this._startBank);
            percentage = percentage * 100;
            Console.WriteLine("Change in percentage: " + percentage + "%");
        }

        private void BuyStocks(decimal closing)
        {
            
            decimal div = this._bank / closing;
            decimal fin = Math.Floor(div);
            decimal tmp = div - fin;
            this._bank = tmp * closing;
            this._stockDepot = this._stockDepot + Convert.ToInt32(fin);
        }

        private void SellStock(decimal closing)
        {
            this._bank = this._bank + this._stockDepot * closing;
            this._stockDepot = 0;
        }

        private void addToSimTable(DateTime timeKey, decimal bank, decimal stockDepot, StockDepotAction act)
        {
            changeTableData dat = new changeTableData();
            dat.bank = bank;
            dat.stockDepot = stockDepot;
            dat.depotAction = act;
            try
            {
                this.changeTable.Add(timeKey, dat);
            }
            catch
            {
                //skippign in case of duplicated keys
            }
        }
        
        public void printChangeTable()
        {
            Console.WriteLine("Table:");
            foreach (var item in this.changeTable)
            {
                Console.Write(item.Key);
                Console.Write("     " + Math.Round(item.Value.bank));
                Console.Write("     " + Math.Round(item.Value.stockDepot));
                switch (item.Value.depotAction)
                {
                    case StockDepotAction.Bought:
                        Console.Write("     " + "Action=Bought");
                        break;
                    case StockDepotAction.Sold:
                        Console.Write("     " + "Action=Sold");
                        break;
                }
                Console.Write(Environment.NewLine);
            }
        }

        public enum StockStrategy
        {
            Avg,
            AvgMargin,
            BuyAndHold
        }

        public enum StockDepotAction
        {
            Sold,
            Bought,
            NoAction
        }
    }

    public struct changeTableData
    {
        public decimal bank;
        public decimal stockDepot;
        public stockSim.StockDepotAction depotAction;
    }
}