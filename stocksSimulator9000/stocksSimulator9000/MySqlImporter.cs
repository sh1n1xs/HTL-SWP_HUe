using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using MySqlConnector;

//this class is used to import data from an mariadb sql server.

namespace stocksSimulator9000
{
    public class MySqlImporter
    {
        private Dictionary<DateTime, StockData> _stockDataCollection = new Dictionary<DateTime, StockData>();
        private string _connectionString;
        private  MySqlConnection _con;
        private string _stockSymbol;
        
        public Dictionary<DateTime, StockData> stockDataCollection
        {
            get
            {
                return this._stockDataCollection;
            }
        }

        public MySqlImporter(string serverIp, string username, string password)
        {
            this._connectionString = @"server=" + serverIp + ";userid="+ username + ";password="+ password + ";database=stonks";
            try
            {
                this._con = new MySqlConnection(this._connectionString);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            
        }


        public void Connect()
        {
            try
            {
                this._con.Open();
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void CloseConnection()
        {
            try
            {
                this._con.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void ImportData(string stockSymbol)
        {
            this._stockSymbol = stockSymbol;
            string sql = "SELECT t1.timeKey, t1.close, t1.split_coefficient, t2.avgAdj from " + this._stockSymbol + " t1 INNER JOIN " + this._stockSymbol + "_misc t2 ON t2.timeKey = t1.timeKey;";
            try
            {
                using var cmd = new MySqlCommand(sql, this._con);
                using MySqlDataReader rdr = cmd.ExecuteReader();
                StockData st = new StockData();
                while (rdr.Read())
                {   
                    st.AdjustedClose = rdr.GetDecimal(1);
                    st.SplitCorrection = rdr.GetDecimal(2);
                    st.AdjustedAvg = rdr.GetDecimal(3);
                    this._stockDataCollection.Add(rdr.GetDateTime(0), st);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
    }

    public struct StockData
    {
        public decimal AdjustedClose;
        public decimal SplitCorrection;
        public decimal AdjustedAvg;
    }
    
}
