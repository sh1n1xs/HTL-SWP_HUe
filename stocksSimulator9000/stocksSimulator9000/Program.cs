﻿using System;
using System.Data;
using System.Linq;
using System.Net.Mime;
using System.Runtime.CompilerServices;
using ScottPlot;

namespace stocksSimulator9000
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             *  -i <ip>
             *  -u <user>
             *  -p <psw>
             *  TO-DO: -g <graph output path>
             *  -r [prints table]
             *  -s <stock symbol>
             *  -d <start date>
             *  -b <starting bank money>
             */
            
            bool nextIsIP = false;
            bool nextUser = false;
            bool nextPsw = false;
            bool nextGraph = false;
            bool nextStock = false;
            bool nextDate = false;
            bool nextBank = false;

            string sqllIP = "127.0.0.1";
            string sqlUser = "user";
            string sqlPsw = "";
            bool printTable = false;
            bool saveGraph = false;
            string graphPath = "";
            string stockSymbol = "ibm";
            string startDateString = "1990-01-01";
            decimal starterBankMoney = 10000;

            foreach (var arg in args)
            {
                if (nextIsIP)
                {
                    sqllIP = arg;
                    nextIsIP = false;
                }
                if (nextUser)
                {
                    sqlUser = arg;
                    nextUser = false;
                }
                if (nextPsw)
                {
                    sqlPsw = arg;
                    nextPsw = false;
                }

                if (nextStock)
                {
                    stockSymbol = arg;
                    stockSymbol = stockSymbol.ToLower();
                    nextStock = false;
                }

                if (nextDate)
                {
                    startDateString = arg;
                    nextDate = false;
                }

                if (nextBank)
                {
                    try
                    {
                        starterBankMoney = Convert.ToDecimal(arg);
                    }
                    catch
                    {
                        Console.WriteLine("invalid input...");
                        Environment.Exit(0);
                    }
                    nextBank = false;
                }
                
                if (nextGraph)
                {
                    saveGraph = true;
                    graphPath = arg;
                    nextGraph = false;
                }
                

                if (arg == "-i")
                {
                    nextIsIP = true;
                }
                if (arg == "-u")
                {
                    nextUser = true;
                }
                if (arg == "-p")
                {
                    nextPsw = true;
                }

                if (arg == "-r")
                {
                    //just set's it to true because no args are needed
                    printTable = true;
                }

                if (arg == "-g")
                {
                    //disabled, because lib is not rendering correctly currently
                    //nextGraph = true;
                }

                if (arg == "-s")
                {
                    nextStock = true;
                }

                if (arg == "-d")
                {
                    nextDate = true;
                }

                if (arg == "-b")
                {
                    nextBank = true;
                }
                
                if (arg == "--help")
                {
                    Console.WriteLine("stockSimulator9000 v1");
                    Console.WriteLine("-i <SQL server IP>");
                    Console.WriteLine("-u <SQL login user>");
                    Console.WriteLine("-p <SQL login password>");
                    Console.WriteLine("-r [prints table]");
                    Console.WriteLine("-s <stock symbol>");
                    Console.WriteLine("-d <start date>");
                    Console.WriteLine("-b <starting bank money>");
                    Environment.Exit(0);
                }

            }

            
            DateTime startDate = DateTime.Now;
            try
            {
                startDate = DateTime.Parse(startDateString);
            }
            catch
            {
                Console.WriteLine("invalid start date argument");
                Environment.Exit(1);
            }
            
            MySqlImporter importer = new MySqlImporter(sqllIP, sqlUser, sqlPsw);
            importer.Connect();
            importer.ImportData(stockSymbol);
            importer.CloseConnection();

            stockSim.StockStrategy[] stratLoop = new stockSim.StockStrategy[3];
            stratLoop[0] = stockSim.StockStrategy.Avg;
            stratLoop[1] = stockSim.StockStrategy.AvgMargin;
            stratLoop[2] = stockSim.StockStrategy.BuyAndHold;
            
            stockSim sts = new stockSim(importer.stockDataCollection, stockSymbol, starterBankMoney);

            foreach (var currentStrat in stratLoop)
            {
                sts.StrategyForSim = currentStrat;
                sts.RunSimu(startDate);
                Console.Write(Environment.NewLine + Environment.NewLine);

                if (printTable)
                {
                    sts.printChangeTable();
                    Console.Write(Environment.NewLine + Environment.NewLine);
                }
                
            }
            
            if (saveGraph)
            {
                //TO-DO create and save graph to hard drive
                DateTime[] timeKey = new DateTime[sts.changeTable.Count +1];
                double[] bankTimeLine = new double[sts.changeTable.Count +1];
                double[] stockDepotLine = new double[sts.changeTable.Count +1];

                int counter = 0;
                foreach (var item in sts.changeTable) //Converts data from the dict to an array for the plotting lib
                {
                    timeKey[counter] = item.Key;
                    bankTimeLine[counter] = Convert.ToDouble(item.Value.bank);
                    stockDepotLine[counter] = Convert.ToDouble(item.Value.stockDepot);
                    counter++;
                }

                var plot = new ScottPlot.Plot();
                double[] timeX = timeKey.Select(x => x.ToOADate()).ToArray();
                plot.AddScatter(timeX, bankTimeLine);
                plot.AddScatter(timeX, stockDepotLine);
                
                plot.Title("Bank and stock depot over time");
                plot.XLabel("Time");
                plot.YLabel("Depot");
                plot.XAxis.DateTimeFormat(true);
                try
                {
                    plot.SaveFig(graphPath);
                }
                catch
                {
                    Console.Write("Unable to save graph....");
                    Environment.Exit(0);
                }

            }
            
        }
    }
}
