﻿using System;
using System.Data;
using System.IO;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Globalization;

/*
 *yeah this was bs
 * mistakes were made
 */

namespace stocksSimulator9000
{
    [Obsolete("Not used", true)]
    public class TableMgr
    {
        #if DEBUG
        private string _connectionString = @"Data Source=table.db;Version=3;"; //for debuggin i'm using an actual file instead of an memory database
        #else
        private string _connectionString = @"Data Source=:memory:;Version=3;New=True;";
        #endif

        private SQLiteConnection _con;

        public TableMgr()
        {
            try
            {
                this._con = new SQLiteConnection(this._connectionString);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            
        }

        public void loadData(string stockSymbol, Dictionary<DateTime, StockData> data)
        {
            //create table for certain stock
            string sql = "create table  IF NOT EXISTS "+ stockSymbol + "(timeKey TEXT, adjustedClose DECIMAL(20,10), splits DECIMAL(20,10), eravg DECIMAL(20,10), PRIMARY KEY (timeKey));";
            var cmd = new SQLiteCommand(this._con);
            try
            {
                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            
            //adding all items from the sql db to the memory sqlite table
            sql = "INSERT INTO "+ stockSymbol +"(timekey, adjustedClose, splits, eravg) VALUES(@timekey, @adjustedClose, @splits, @eravg);";
            try
            {
                foreach (var item in data)
                {
                    cmd.CommandText = sql;
                    cmd.Parameters.AddWithValue("@timekey", item.Key.ToString("yyyy-MM-dd HH:mm:ss"));
                    cmd.Parameters.AddWithValue("@adjustedClose",item.Value.AdjustedClose);
                    cmd.Parameters.AddWithValue("@splits", item.Value.SplitCorrection);
                    cmd.Parameters.AddWithValue("@eravg", item.Value.AdjustedAvg);
                    cmd.Prepare();
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        /*
         *TO-DO
         * get data struct by key
         * create simulaton table(s) (timekey, bank decimal, stocks depot int)
         * write to simulaton table by key
         * write simulation
         */
        
        public void Connect()
        {
            this._con.Open();
        }

        public void CloseConnection()
        {
            this._con.Close();
        }
        
    }
}