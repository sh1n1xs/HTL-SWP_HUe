# stockSimulator9000

stockSimulator9000 is a program to import your data from and stocksCLI via an sql database.

## Usage

First you have to specify sql connection data as parameters:
```batch
    -i <SQL server IP>
    -u <SQL login user>
    -p <SQL login password>
    -s <stock symbol>
    -d <start date>
    -b <bank starting money>
    -r [prints table]
```

All other instructions will be displayed in the program.


