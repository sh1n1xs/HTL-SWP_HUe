# stonksCLI

![Image](stonks_banner.png)

stonksCLI is a java command line tool to import data from [alphavantage](https://www.alphavantage.co/) to a database.
You can visualize the data using an included javafx application.

For more information see usage.

stonksCLI is wip.

## Usage

stonksCLI uses the following parameters:

```batch
stonksCLI [-?] [-a <arg>] [-d <arg>] [-g] [-i <arg>] [-l <arg>] [-p <arg>] [-s <arg>] [-u] [-v]

-g, --gui
	Outputs data in an javaFX diagram.

-u, --update
	Gets data from alphavantage and imports it into a database.

-i, --mariadb-ip <arg>
    Parameter for the MariaDB Server IP.
    Default value is "127.0.0.1".
    
-l, --login-user <arg>
    Parameter for the username used to login to the MariaDB Server.
    Default value is "root".

-p, --psw <arg>
    Parameter for the password used to login to the MariaDB Server.
    Default value is "".

-a, --api-key <arg>
    Parameter for the alphavantage api key.
    Is required for "--update".

-s, --stock-symbol <arg>
    Parameter for the stock symbol.
    Required for "--update" and "--gui".
    Default value for testing reasons is "IBM".
    
-d, --display-Days <arg>
    Parameter for a full Number, that defines how many days are displayed in the chart.
    Required for "--gui".
    Default value is "356".
    
-v, --version
    Prints program version and then therminates.

-c, --scr <arg>
	Saves a screenshot of the graph as png. Path and filename are required as arguments.
	After the screenshot was saved, the application will exit out immediately without displaying a gui.
	If you want to display the gui too,  you have to run the application twice!
	Requires "--gui".
    
-?, --help
	Displays help. If no parameter is given, help is displayed too.

```

### Example:

In the following example we used the following parameters to import data to the database:

```batch
 stonksCLI -u -i "127.0.0.1" -l "root" -p "totalyRealPassword" -a "totalyRealApiKey" -s "IBM"
```

To display the imported data, we can use the following parameters:

```batch
 stonksCLI -g -d 20 -i "127.0.0.1" -l "root" -p "totalyRealPassword" -s "IBM"
```

With that we get the following output:

![Image](exampleScr.png)

![Image](exampleScr2.png)

