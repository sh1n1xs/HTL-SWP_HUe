package com.stonks;

import java.io.File;
import java.sql.*;

/*
 * To-DO
 * - create tables
 * - write stuff into table
 */

@Deprecated
class dbMgrSQLITE {
    private alphavantage av;
    private String dbName;

    public dbMgrSQLITE(alphavantage alphavantageOBJ, String filename) {
        this.av = alphavantageOBJ;
        this.dbName = filename;
        File f = new File(System.getProperty("user.dir") + System.getProperty("file.separator") + this.dbName);

        String createTableSQL = "CREATE TABLE IF NOT EXISTS " + this.av.metaData.get("2. Symbol") + "(timeKey TEXT PRIMARY KEY NOT NULL, open TEXT, high TEXT, low TEXT, close TEXT, adjusted_close TEXT, volume TEXT, dividend_amount TEXT, split_coefficient TEXT);";
        //System.out.println(createTableSQL);
        if (!f.exists()) //creates db if it doesn't exist
        {
            try {
                String url = "jdbc:sqlite:"  + System.getProperty("user.dir") + System.getProperty("file.separator") + this.dbName;
                //System.out.println(url);
                Connection conn = DriverManager.getConnection(url);
                if (conn != null) {
                    DatabaseMetaData meta = conn.getMetaData();
                    Statement stm = conn.createStatement();
                    stm.executeUpdate(createTableSQL);
                    stm.close();
                    conn.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public void updateDB() {
        //test...
        //add iterator through time keys + add in sql on update
        String open = "";
        String high = "";
        String low = "";
        String close = "";
        String adjusted_close = "";
        String volume = "";
        String dividend_amount = "";
        String split_coefficient = "";
        for (String keyStr : this.av.timeSeries.keySet()) {
            open = this.av.getDataByDate("open", keyStr);
            high = this.av.getDataByDate("high", keyStr);
            low = this.av.getDataByDate("low", keyStr);
            close = this.av.getDataByDate("close", keyStr);
            adjusted_close = this.av.getDataByDate("adjusted_close", keyStr);
            volume = this.av.getDataByDate("volume", keyStr);
            dividend_amount = this.av.getDataByDate("dividend_amount", keyStr);
            split_coefficient = this.av.getDataByDate("split_coefficient", keyStr);
            writeLineToDB(keyStr, open, high, low, close, adjusted_close, dividend_amount, volume, split_coefficient);
        }
    }

    private void writeLineToDB(String timeKey, String open, String high, String low, String close, String adjusted_close, String dividend_amount, String volume, String split_coefficient)
    {
        String sql = "INSERT INTO " + this.av.metaData.get("2. Symbol") + "(timeKey, open, high, low, close, adjusted_close, volume, dividend_amount, split_coefficient) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try {
            String url = "jdbc:sqlite:"  + System.getProperty("user.dir") + System.getProperty("file.separator") + this.dbName;
            System.out.println(url);
            Connection conn = DriverManager.getConnection(url);
            if (conn != null) {
                    PreparedStatement pstmt = conn.prepareStatement(sql);
                    pstmt.setString(1, timeKey);
                    pstmt.setString(2, open);
                    pstmt.setString(3, high);
                    pstmt.setString(4, low);
                    pstmt.setString(5, close);
                    pstmt.setString(6, adjusted_close);
                    pstmt.setString(7, volume);
                    pstmt.setString(8, dividend_amount);
                    pstmt.setString(9, split_coefficient);
                    pstmt.executeUpdate();
                    conn.close();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }


    }


}
