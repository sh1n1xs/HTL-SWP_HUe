package com.stonks;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class dbMgrMySQL {

    private alphavantage av;
    private String ip;
    private String user;
    private String pass;
    private String databasename = "stonks";
    private String DB_URL;

    private final String JDBC_DRIVER = "org.mariadb.jdbc.Driver";

    public dbMgrMySQL(String serverIP, String loginUser, String userPsw) {
        this.ip = serverIP;
        this.DB_URL = "jdbc:mariadb://" + this.ip + "/" + this.databasename;
        this.user = loginUser;
        this.pass = userPsw;

        //Connect to db for testing reasons
        Connection conn = null;
        Statement stmt = null;
        try
        {
            Class.forName("org.mariadb.jdbc.Driver");
            conn  = DriverManager.getConnection(this.DB_URL, this.user, this.pass);

            //just a quick test if it works
            /*
            stmt = conn.createStatement();
            String sql = "create table test(test INTEGER);";
            stmt.executeUpdate(sql);
            */

        }catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
        }finally {
            try {
                if (stmt != null) {
                    conn.close();
                }
            } catch (SQLException e)
            {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        //End test
    }
    public void saveToDB(alphavantage av)
    {
        Connection conn = null;
        Statement stmt = null;
        try
        {
            Class.forName("org.mariadb.jdbc.Driver");
            conn  = DriverManager.getConnection(this.DB_URL, this.user, this.pass);


            stmt = conn.createStatement();
            String sql = "CREATE TABLE IF NOT EXISTS " + av.metaData.get("2. Symbol")  + "(timeKey DATETIME NOT NULL, open DECIMAL(14,6), high DECIMAL(14,6), low DECIMAL(14,6), close DECIMAL(14,6), adjusted_close DECIMAL(14,6), volume INT, dividend_amount DECIMAL(14,6), split_coefficient DECIMAL(14,6), PRIMARY KEY(timekey));";
            stmt.executeUpdate(sql); //creates the table if it doesn't exist already
            sql = "CREATE TABLE IF NOT EXISTS "+ av.metaData.get("2. Symbol") + "_misc(timeKey DATETIME NOT NULL ,split DECIMAL(14,6), avgAdj DECIMAL(14,6), PRIMARY KEY(timekey));";
            stmt.executeUpdate(sql); //creates the table if it doesn't exist already
        }catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
        }finally {
            try {
                if (stmt != null) {
                    conn.close();
                }
            } catch (SQLException e)
            {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }

        }

        //insert here: for-loop for av timekeys stuff that calls writeToDB all the time
        String open = "";
        String high = "";
        String low = "";
        String close = "";
        String adjusted_close = "";
        String volume = "";
        String dividend_amount = "";
        String split_coefficient = "";

        for (String keyStr : av.timeSeries.keySet()) {
            open = av.getDataByDate("open", keyStr);
            high = av.getDataByDate("high", keyStr);
            low = av.getDataByDate("low", keyStr);
            close = av.getDataByDate("close", keyStr);
            adjusted_close = av.getDataByDate("adjusted_close", keyStr);
            volume = av.getDataByDate("volume", keyStr);
            dividend_amount = av.getDataByDate("dividend_amount", keyStr);
            split_coefficient = av.getDataByDate("split_coefficient", keyStr);
            this.writeToDB(av, keyStr, Double.parseDouble(open), Double.parseDouble(high), Double.parseDouble(low), Double.parseDouble(close), Double.parseDouble(adjusted_close), Integer.parseInt(volume), Double.parseDouble(dividend_amount), Double.parseDouble(split_coefficient));
        }



    }

    private void writeToDB(alphavantage av, String timekey, double open, double high, double low, double close, double adjusted_close, int volume, double dividend_amount, double split_coefficient)
    {
        Connection conn = null;
        Statement stmt = null;
        try
        {
            Class.forName("org.mariadb.jdbc.Driver");
            conn  = DriverManager.getConnection(this.DB_URL, this.user, this.pass);


            stmt = conn.createStatement();
            String sql = "INSERT INTO " + av.metaData.get("2. Symbol") + "(timeKey, open, high, low, close, adjusted_close, volume, dividend_amount, split_coefficient) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE open = VALUES(open),  high = VALUES(high), low = VALUES(low), close = VALUES(close), adjusted_close = VALUES(adjusted_close), volume = VALUES(volume), dividend_amount = VALUES(dividend_amount), split_coefficient = VALUES(split_coefficient);";
            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setDate(1, java.sql.Date.valueOf(timekey));
            preparedStmt.setDouble(2, open);
            preparedStmt.setDouble(3, high);
            preparedStmt.setDouble(4, low);
            preparedStmt.setDouble(5, close);
            preparedStmt.setDouble(6, adjusted_close);
            preparedStmt.setInt(7, volume);
            preparedStmt.setDouble(8, dividend_amount);
            preparedStmt.setDouble(9, split_coefficient);
            preparedStmt.executeUpdate();

        }catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
        }finally {
            try {
                if (stmt != null) {
                    conn.close();
                }
            } catch (SQLException e)
            {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public void writeMiscTable(HashMap<Date,Double> splitcorrected, HashMap<Date, Double> avg, String stockSymbol)
    {

        Connection conn = null;
        Statement stmt = null;
        try
        {
            Class.forName("org.mariadb.jdbc.Driver");
            conn  = DriverManager.getConnection(this.DB_URL, this.user, this.pass);

            for (Date timekey : splitcorrected.keySet()){
            stmt = conn.createStatement();
            String sql = "INSERT INTO " + stockSymbol + "_misc" + "(timeKey, split, avgAdj) VALUES(?, ?, ?) ON DUPLICATE KEY UPDATE split = VALUES(split), avgAdj = VALUES(avgAdj);";
            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setDate(1, java.sql.Date.valueOf(String.valueOf(timekey)));
            preparedStmt.setDouble(2, splitcorrected.get(timekey));
            preparedStmt.setDouble(3, avg.get(timekey));
            preparedStmt.executeUpdate();

            }

        }catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
        }finally {
            try {
                if (stmt != null) {
                    conn.close();
                }
            } catch (SQLException e)
            {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public double getAverage(String stockSymbol, Date avgDate)
    {
        double avg = -1;
        String sql = "SELECT AVG(close) FROM " + stockSymbol + " WHERE timeKey BETWEEN DATE_SUB(?, INTERVAL 200 DAY) AND ?;";
        Connection conn = null;
        Statement stmt = null;
        try
        {
            Class.forName("org.mariadb.jdbc.Driver");
            conn  = DriverManager.getConnection(this.DB_URL, this.user, this.pass);


            stmt = conn.createStatement();
            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setDate(1, java.sql.Date.valueOf(String.valueOf(avgDate)));
            preparedStmt.setDate(2, java.sql.Date.valueOf(String.valueOf(avgDate)));
            ResultSet rs = preparedStmt.executeQuery();
            while (rs.next())
            {
                avg = rs.getDouble("AVG(close)");
            }

        }catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
        }finally {
            try {
                if (stmt != null) {
                    conn.close();
                }
            } catch (SQLException e)
            {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }

        return avg;
    }

    public double getTotalDataEntries(String stockSymbolt)
    {
        double totalDatadays = 0;
        String sql = "SELECT COUNT(timekey) AS days from " + stockSymbolt + ";";
        Connection conn = null;
        Statement stmt = null;
        try
        {
            Class.forName("org.mariadb.jdbc.Driver");
            conn  = DriverManager.getConnection(this.DB_URL, this.user, this.pass);

            stmt = conn.createStatement();
            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            ResultSet rs = preparedStmt.executeQuery();
            while (rs.next())
            {
                totalDatadays = rs.getDouble("days");
            }

        }catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
        }finally {
            try {
                if (stmt != null) {
                    conn.close();
                }
            } catch (SQLException e)
            {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return totalDatadays;
    }

    public HashMap<Date, Double> getClosing(String stockSymbol, int daysOfData)
    {
        HashMap<Date, Double> closingData = new HashMap<Date, Double>();

        String sql = "SELECT timekey, adjusted_close FROM " + stockSymbol + " WHERE timeKey BETWEEN DATE_SUB(CURRENT_DATE(), INTERVAL ? DAY) AND CURRENT_DATE();";
        Connection conn = null;
        Statement stmt = null;
        try
        {
            Class.forName("org.mariadb.jdbc.Driver");
            conn  = DriverManager.getConnection(this.DB_URL, this.user, this.pass);

            stmt = conn.createStatement();
            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setInt(1, daysOfData);
            ResultSet rs = preparedStmt.executeQuery();
            while (rs.next())
            {
                closingData.put(rs.getDate("timekey"), rs.getDouble("adjusted_close"));
            }

        }catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
        }finally {
            try {
                if (stmt != null) {
                    conn.close();
                }
            } catch (SQLException e)
            {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }


        return closingData;
    }

    public HashMap<Date, Double> getAdjustedClosing(String stockSymbol, int daysOfData)
    {
        HashMap<Date, Double> closingData = new HashMap<Date, Double>();

        String sql = "SELECT timekey, adjusted_close FROM " + stockSymbol + " WHERE timeKey BETWEEN DATE_SUB(CURRENT_DATE(), INTERVAL ? DAY) AND CURRENT_DATE();";
        Connection conn = null;
        Statement stmt = null;
        try
        {
            Class.forName("org.mariadb.jdbc.Driver");
            conn  = DriverManager.getConnection(this.DB_URL, this.user, this.pass);

            stmt = conn.createStatement();
            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setInt(1, daysOfData);
            ResultSet rs = preparedStmt.executeQuery();
            while (rs.next())
            {
                closingData.put(rs.getDate("timekey"), rs.getDouble("adjusted_close"));
            }

        }catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
        }finally {
            try {
                if (stmt != null) {
                    conn.close();
                }
            } catch (SQLException e)
            {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }


        return closingData;
    }

    public HashMap<Date, Double> getAvgByClosing(HashMap<Date, Double> closingData, String stockSymbol)
    {
        HashMap<Date,Double> avgByClosing = new HashMap<Date,Double>();
        for(Date key : closingData.keySet())
        {
            avgByClosing.put(key, this.getAverage(stockSymbol,key));
        }
        return avgByClosing;
    }

}
