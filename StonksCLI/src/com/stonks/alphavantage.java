package com.stonks;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;

import org.json.*;





public class alphavantage {
    private JSONObject timeSeriesOBJ;
    private JSONObject metaDataOBJ;

    public HashMap<String, String> timeSeries =  new HashMap<String, String>();
    public HashMap<String, String> metaData =  new HashMap<String, String>();

    public alphavantage(String alphavantageApiKey, String symbol)
    {
        String apiKey = alphavantageApiKey;
        //String apiKey = getAPIKey();
        if (apiKey == null)
        {
            //throw new FileNotFoundException(); //throws an exception to handle later in main
            System.out.println("can't get api key from file");
            return;
        }
        String JSONtxt = "";
        try
        {
            JSONtxt = getHTML("https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&outputsize=full&symbol=" + symbol.toUpperCase() + "&apikey=" + apiKey);
        }catch(Exception e)
        {
            System.out.println("unable to reach alphavantage");
        }

        JSONObject jsonObj = new JSONObject(JSONtxt);
        this.metaDataOBJ = (JSONObject) jsonObj.get("Meta Data");
        this.timeSeriesOBJ = (JSONObject) jsonObj.get("Time Series (Daily)");

        for (String keyStr : this.metaDataOBJ.keySet()) {
            Object keyvalue = this.metaDataOBJ.get(keyStr);
            this.metaData.put(keyStr, keyvalue.toString());
        }
        for (String keyStr : this.timeSeriesOBJ.keySet()) {
            Object keyvalue = this.timeSeriesOBJ.get(keyStr);
            this.timeSeries.put(keyStr, keyvalue.toString());
        }

    }

    public String getDataByDate(String dataType, String dateKey)
    {
        String key = "";
        switch (dataType) {
            case "open":
                key = "1. open";
                break;
            case "high":
                key = "2. high";
                break;
            case "low":
                key = "3. low";
                break;
            case "close":
                key = "4. close";
                break;
            case "adjusted_close":
                key = "5. adjusted close";
                break;
            case "volume":
                key = "6. volume";
                break;
            case "dividend_amount":
                key = "7. dividend amount";
                break;
            case "split_coefficient":
                key = "8. split coefficient";
                break;
        }

        String JSONtxt = this.timeSeries.get(dateKey);
        JSONObject jobj = new JSONObject(JSONtxt);
        return jobj.get(key).toString();
    }

    @Deprecated
    private String getAPIKey()
    {
        String text = "";
        try
        {
            String filename = System.getProperty("user.home") + System.getProperty("file.separator") + "alphavantage.txt"; //reads the file from the home directory of the user, should work on win+lnx
            BufferedReader read = new BufferedReader(new FileReader(filename));
            text = read.readLine();
        }catch (IOException e)
        {
            return null;
        }
        return text;
    }

    private String getHTML(String urlToRead) throws Exception {
        StringBuilder result = new StringBuilder();
        URL url = new URL(urlToRead);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        rd.close();
        return result.toString();
    }
}
