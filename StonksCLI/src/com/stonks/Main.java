package com.stonks;


import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;
import java.util.Calendar;


import java.io.PrintWriter;

import org.apache.commons.cli.*;

import javax.swing.text.StyledEditorKit;

public class Main {

    public static void main(String[] args) {

        Options options = new Options();
        options.addOption("g","gui",false,"Displays data from source in gui application");
        options.addOption("u","update",false,"Imports data from alphavantage to database");
        options.addOption("i","mariadb-ip", true, "Inputs MariaDB data source ip address");
        options.addOption("l", "login-user", true, "Inputs username for MariaDB login");
        options.addOption("p","psw", true,"Inputs login password for MariaDB");
        options.addOption("a","api-key",true,"Inputs alphavantage api key");
        options.addOption("s","stock-symbol",true, "Inputs stock symbol for database import/export");
        options.addOption("v","version", false,"Shows program version");
        options.addOption("d","display-Days",true,"Inputs how many days should be displayed in the chart");
        options.addOption("?", "help",false,"Show help");
        options.addOption("c", "scr", true, "Saves a screenshot");

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd;
        try
        {
            cmd = parser.parse(options, args);
        }catch (ParseException e)
        {
            System.out.println(e.getMessage());
            return;
        }

        if (cmd.hasOption('v'))
        {
            System.out.println("stonksCLI Version 1.0");
            return;
        }

        //Stocks stuff
        String stockSymbol = "IBM"; //fallback because reasons...
        String apiKey = "";


        //Database Stuff
        String mdbIP = "127.0.0.1";
        String mdbUser = "root";
        String mdbPsw = "";

        //Diagram
        int drawDays = 356;
        String screenshotPath = "";
        Boolean saveScr = false;

        Boolean showArguments = true;

        if (cmd.hasOption('a'))
        {
            apiKey = cmd.getOptionValue('a');
            showArguments = false;
        }
        if (cmd.hasOption('s'))
        {
            stockSymbol = cmd.getOptionValue('s');
            showArguments = false;
        }
        if (cmd.hasOption('i'))
        {
            mdbIP = cmd.getOptionValue('i');
            showArguments = false;
        }
        if (cmd.hasOption('p'))
        {
            mdbPsw = cmd.getOptionValue('p');
            showArguments = false;
        }
        if (cmd.hasOption('l'))
        {
            mdbUser = cmd.getOptionValue('l');
            showArguments = false;
        }
        if (cmd.hasOption('c'))
        {
            saveScr = true;
            screenshotPath = cmd.getOptionValue('c');
            if (screenshotPath.equals(null))
            {
                saveScr = false;
            }
        }
        if (cmd.hasOption('d'))
        {
            try
            {
                drawDays = Integer.parseInt(cmd.getOptionValue('d'));
                showArguments = false;
            }catch(Exception e)
            {
                System.out.println("Unvalid day number");
                System.out.println(e.getMessage());
                return;
            }
        }

        if ((showArguments == true) || (cmd.hasOption('?')))
        {
            HelpFormatter formatter = new HelpFormatter();
            final PrintWriter writer = new PrintWriter(System.out);
            formatter.printUsage(writer,80,"CLITester", options);
            writer.flush();
            return;
        }

        //creates db, if failes exit
        dbMgrMySQL db;
        try
        {
            db = new dbMgrMySQL(mdbIP,mdbUser, mdbPsw);
        }catch (Exception e)
        {
            System.out.println("Can't connect to MariaDB");
            System.out.println(e.getMessage());
            return;
        }

        if (cmd.hasOption('u'))
        {
            if (apiKey == "")
            {
                System.out.println("API key unknown!");
                return;
            }
            import_to_db(apiKey, stockSymbol, db);
        }

        if (cmd.hasOption('g'))
        {
            gui_draw_diagram(db, stockSymbol, drawDays, saveScr, screenshotPath);
        }
        return;
    }

    private  static void import_to_db(String apikey, String stockSymbol, dbMgrMySQL db)
    {
        try
        {
            alphavantage av = new alphavantage(apikey, stockSymbol);
            db.saveToDB(av);
            int totalDataEntries = (int)db.getTotalDataEntries(stockSymbol);
            var closingHM = db.getAdjustedClosing(stockSymbol, totalDataEntries);
            var AVGbyClosingHM = db.getAvgByClosing(closingHM, stockSymbol);
            db.writeMiscTable(closingHM,AVGbyClosingHM,stockSymbol);
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
            return;
        }
    }
    private static void gui_draw_diagram(dbMgrMySQL db, String stockSymbol, int drawDays, Boolean saveScr, String scrPath)
    {
        //var closingHM = db.getClosing(stockSymbol, drawDays);
        var closingHM = db.getAdjustedClosing(stockSymbol, drawDays);
        var AVGbyClosingHM = db.getAvgByClosing(closingHM, stockSymbol);
        Diagram.main(null, closingHM, AVGbyClosingHM, stockSymbol, saveScr, scrPath); //not a good way of doing things but it works
    }

}
