package com.stonks;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Node;
import javafx.scene.image.WritableImage;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.util.HashMap;
import java.util.TreeMap;

public class Diagram extends Application {

    private static String stockSymbol = "";
    private  static HashMap<Date,Double> closing;
    private  static HashMap<Date,Double> avg;
    private  static Boolean saveScr = false;
    private  static String scrPath = "";

    public static void main(String[] args, HashMap<Date, Double> closingHM, HashMap<Date,Double> avgHM, String symbol, Boolean saveScreen, String screenshotPath)
    {
        stockSymbol = symbol;
        closing = closingHM;
        avg = avgHM;

        if (saveScreen.equals(true))
        {
            saveScr = true;
            scrPath = screenshotPath;
        }else {
            saveScr = false;
            scrPath = "";
        }
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        stage.setTitle("StonksCLI - gui: " + "\"" + stockSymbol + "\"");
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Day");
        yAxis.setAutoRanging(false);
        yAxis.setUpperBound(calcUpperBound());
        yAxis.setLowerBound(calcLowerBound());
        final LineChart<String,Number> lineChart =
                new LineChart<String,Number>(xAxis,yAxis);
        lineChart.setCreateSymbols(false);
        lineChart.setTitle(stockSymbol);

        XYChart.Series series1 = new XYChart.Series();
        series1.setName(stockSymbol + " closing (blue)");

        XYChart.Series series2 = new XYChart.Series();
        series2.setName(stockSymbol + " average (black)");

        //converts into treemap for ez sorting yay
        TreeMap<Date, Double> closingSorted = new TreeMap<Date, Double>();
        closingSorted.putAll(closing);

        for (Date key : closingSorted.keySet())
        {
            series1.getData().add(new XYChart.Data(key.toString(), closing.get(key)));

            //average stuff:
            if (avg.get(key) != null)
            {
                series2.getData().add(new XYChart.Data(key.toString(), avg.get(key)));
            }
        }


        Scene scene  = new Scene(lineChart,800,600);
        lineChart.getData().addAll(series1, series2);
        //lineChart.setStyle("CHART_COLOR_1: blue ;");
        series1.getNode().lookup(".chart-series-line").setStyle("-fx-stroke: blue;");
        series2.getNode().lookup(".chart-series-line").setStyle("-fx-stroke: black;");


        Date lastEntry = closingSorted.lastKey();
        if (closing.get(lastEntry) > avg.get(lastEntry))
        {
            lineChart.lookup(".chart-plot-background").setStyle("-fx-background-color: transparent;");
            lineChart.setStyle("-fx-background-color: #0ef898;");
        }else {
            lineChart.lookup(".chart-plot-background").setStyle("-fx-background-color: transparent;");
            lineChart.setStyle("-fx-background-color: #fa6f50;");
        }

        if (saveScr.equals(true))
        {
            WritableImage img = scene.snapshot(null);
            File file = new File(scrPath);
            try
            {
                ImageIO.write(SwingFXUtils.fromFXImage(img, null), "PNG", file);
            }catch(IOException e)
            {
                System.out.println(e.getMessage());
            }
            Platform.exit(); //exit after scr was saved, because it might run as a service
            System.exit(0);
        }

        stage.setScene(scene);
        stage.show();

    }

    private static Double calcLowerBound()
    {
        double lbound = 0;
        double lowClosing = Double.MAX_VALUE;
        for (Date key : closing.keySet())
        {
            if (closing.get(key) < lowClosing)
            {
                lowClosing = closing.get(key);
            }
        }

        double lowAvg = Double.MAX_VALUE;
        for (Date key : avg.keySet())
        {
            if (avg.get(key) < lowAvg)
            {
                lowAvg = avg.get(key);
            }
        }

        if (lowClosing < lowAvg)
        {
            lbound = (lowClosing / 100.0) * 90.0;
        }else {
            lbound = (lowAvg / 100.0) * 90.0;
        }

        return Math.floor(lbound);
    }

    private static Double calcUpperBound()
    {
        double ubound = Double.MAX_VALUE;

        double highClosing = 0;
        for (Date key : closing.keySet())
        {
            if (closing.get(key) > highClosing)
            {
                highClosing = closing.get(key);
            }
        }

        double highAvg = 0;
        for (Date key : avg.keySet())
        {
            if (avg.get(key) > highAvg)
            {
                highAvg = avg.get(key);
            }
        }

        if (highClosing > highAvg)
        {
            ubound = (highClosing / 100.0) * 110.0;
        }else {
            ubound = (highAvg / 100.0) * 110.0;
        }

        return Math.ceil(ubound);
    }


}
