#adds a cronjob that runs the scrip every monday at 1am
#if you want to use this, please change the path to the script file and make sure it's executable
crontab -l | { cat; echo "0 01 * * 2 /path/to/script/stonksCLI_service.sh"; } | crontab -