#!/bin/bash
jar_name="stonksCLI.jar"
args_ip="127.0.0.1"
args_login="stonksroot"
args_psw="some_pass"
args_apikey="api-key"
args_display_days=20
args_scr_path="/tmp"
current_date=`date +"%Y-%m-%d_%T"`
fx_path="/usr/lib/jvm/default/lib"
while IFS= read -r line; do
  args_stock="$line"
  args_scr=${args_scr_path}/${args_stock}_${current_date}.png
  
  echo importing to db for $args_stock
  java -jar "${jar_name}" -u -i $args_ip -l $args_login -p $args_psw -a $args_apikey -s $args_stock
  echo saving graph screenshot for $args_stock
  java  --module-path "${fx_path}" --add-modules=javafx.controls,javafx.fxml,javafx.base  -jar "${jar_name}" -g -d $args_display_days -i $args_ip -l $args_login -p $args_psw -s $args_stock -c $args_scr
done < stock_list.txt
