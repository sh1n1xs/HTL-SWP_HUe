package sample;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.*;

import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;
import com.google.gson.*;

public class HolidayCalc {
    private int startYear;
    private int endYear;
    public Map<String, String> holidays = new Hashtable<>();
    public int Mon = 0, Tue = 0, Wed = 0, Thu = 0, Fri = 0, Sat = 0, Sun = 0;

    public HolidayCalc(int startingYear, int endingYear) {
        this.startYear = startingYear;
        this.endYear = endingYear;
        String holidaysJSON = "";
        for (int i = this.startYear; i <= this.endYear; i++) //goes through every year and adds the holidays to the Dictionary
        {
            try {
                holidaysJSON = getHTML("https://feiertage-api.de/api/?jahr=" + i + "&nur_land=BY");
            } catch (Exception e) {
                System.out.println("Can't reach api"); //to-do: change exception handling lol
                System.exit(1);                  //       because force-quitting everything is stupid
            }
            JsonObject holj = new Gson().fromJson(holidaysJSON, JsonObject.class);
            Gson gson = new Gson();
            Map<String, JsonElement> holiM = gson.fromJson(holidaysJSON, Map.class);



            //iterate through map to put all the cool stuff into the dict, nice
            Iterator<String> it  =  holiM.keySet().iterator();
            while( it.hasNext() ){
                String key = it.next();
                //System.out.println("Key:: !!! >>> "+key);
                Object value = holiM.get(key);

                String valueDate = getDate(value.toString());

                //System.out.println("Value Type "+value.getClass().getName());
                //Makes new key
                if (!key.equals("Tag der Deutschen Einheit"))
                {
                    String nKey = key + " " + String.valueOf(i);
                    String nVal = valueDate;
                    //System.out.println("Valu " + nVal + "\n");
                    holidays.put(nKey, nVal);
                }
            }
            holidays.put("Österreichischer Nationalfeiertag" + " " + String.valueOf(i), String.valueOf(i) + "-10-25");
        }

        //iterates through the Dictionary

        Iterator<String> it = this.holidays.keySet().iterator();
        while(it.hasNext())
        {
            String key=it.next();
            //System.out.println("key::: " + key + " value::: " + this.holidays.get(key));
            LocalDate date = LocalDate.parse(this.holidays.get(key));
            switch (date.getDayOfWeek()) //adds +1 to int per day
            {
                case MONDAY:
                    this.Mon++;
                    break;
                case TUESDAY:
                    this.Tue++;
                    break;
                case WEDNESDAY:
                    this.Wed++;
                    break;
                case THURSDAY:
                    this.Thu++;
                    break;
                case FRIDAY:
                    this.Fri++;
                    break;
                case SATURDAY:
                    this.Sat++;
                    break;
                case SUNDAY:
                    this.Sun++;
                    break;
            }
        }


    }

    private String getHTML(String urlToRead) throws Exception {
        StringBuilder result = new StringBuilder();
        URL url = new URL(urlToRead);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        rd.close();
        return result.toString();
    }


    private String getDate(String value) //groaßa pfusch, i hass mei leben
    {
        String nV = "";
        int startPos = 0;
        boolean startHasFound = false;
        for (int i = 0; i < value.length(); i++) //get start position
        {
            if (value.charAt(i) == '=')
            {
                if (!startHasFound)
                {
                    startPos = i;
                    startHasFound = true;
                }
            }
        }

        int endPos = 0;
        boolean endHasFound = false;
        for (int i = 0; i < value.length(); i++) //get end position
        {
            if (value.charAt(i) == ',')
            {
                if (!endHasFound)
                {
                    endPos = i;
                    endHasFound = true;
                }
            }
        }

        //build new string
        for (int i = startPos+1; i<=endPos-1;i++)
        {
            nV = nV + String.valueOf(value.charAt(i));
        }


        return nV;
    }

}






