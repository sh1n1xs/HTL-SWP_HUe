package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        int startYear = 2020;
        int endYear = 2030;

        HolidayCalc hc = new HolidayCalc(startYear, endYear);
        primaryStage.setTitle("Holiday Calc");

        CategoryAxis xAxis    = new CategoryAxis();
        xAxis.setLabel("Wochentage");

        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Holidays");

        BarChart barChart = new BarChart(xAxis, yAxis);

        XYChart.Series dataSeries1 = new XYChart.Series();
        dataSeries1.setName("From " + String.valueOf(startYear) + " to " + String.valueOf(endYear));

        dataSeries1.getData().add(new XYChart.Data("Monday", hc.Mon));
        dataSeries1.getData().add(new XYChart.Data("Tuesday"  , hc.Tue));
        dataSeries1.getData().add(new XYChart.Data("Wednesday"  , hc.Wed));
        dataSeries1.getData().add(new XYChart.Data("Thursday"  , hc.Thu));
        dataSeries1.getData().add(new XYChart.Data("Friday"  , hc.Fri));
        dataSeries1.getData().add(new XYChart.Data("Saturday"  , hc.Sat));
        dataSeries1.getData().add(new XYChart.Data("Sunday"  , hc.Sun));

        barChart.getData().add(dataSeries1);

        VBox vbox = new VBox(barChart);

        Scene scene = new Scene(vbox, 400, 200);

        primaryStage.setScene(scene);
        primaryStage.setHeight(400);
        primaryStage.setWidth(1200);

        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
